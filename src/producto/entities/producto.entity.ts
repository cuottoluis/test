import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Producto { 

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public nombre_producto: String;

    @Column()
    public descripcion: String;

    @Column()
    public precio_sin_iva: number;

    @Column()
    public precio_con_iva: number;

    @Column()
    public id_tipo_iva: number;

    @Column({ default: 'now()' })
    public fecha_registro: Date;
    
}
