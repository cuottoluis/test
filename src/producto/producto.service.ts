import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { CreateProductoDto } from './dto/create-producto.dto';
import { UpdateProductoDto } from './dto/update-producto.dto';
import { Producto } from './entities/producto.entity';
import { TipoIvaService } from "../tipo-iva/tipo-iva.service";
import { take } from 'rxjs';

@Injectable()
export class ProductoService {

  constructor(
    @InjectRepository(Producto)
    private productoRepository: Repository<Producto>,
    private tipoIvaService: TipoIvaService,
  ) { }

  async create(createProductoDto: CreateProductoDto): Promise<Producto> {
    try {
      var rowTipoIva = await this.tipoIvaService.findOne(createProductoDto.id_tipo_iva);
      createProductoDto.precio_sin_iva
      createProductoDto.precio_con_iva = (createProductoDto.precio_sin_iva + ((rowTipoIva.valor_porcentaje_iva * createProductoDto.precio_sin_iva) / 100));
      var nuevoTipoIva = await this.productoRepository.save(createProductoDto);
      return nuevoTipoIva;
    } catch (error) {
      return error;
    }
  }

  async findAll(limit: number, offset: number): Promise<Producto[]> {
    try {
      return await this.productoRepository.find({
        skip: offset,
        take: limit
      });
    } catch (error) {
      return error;
    }
  }

  async findOne(id: number): Promise<Producto> {
    try {
      return await this.productoRepository.findOne(id);
    } catch (error) {
      return error;
    }
  }

  async findByName(name: string): Promise<Producto> {
    try {
      const producto = getRepository(Producto)
        .createQueryBuilder("producto")
        .where("producto.nombre_producto = :nombre_producto", { nombre_producto: name })
        .getOne();
      return producto;
    } catch (error) {
      return error;
    }
  }

  update(id: number, updateProductoDto: UpdateProductoDto) {
    return `This action updates a #${id} producto`;
  }

  remove(id: number) {
    return `This action removes a #${id} producto`;
  }
}
