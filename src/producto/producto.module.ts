import { Module } from '@nestjs/common';
import { ProductoService } from './producto.service';
import { ProductoController } from './producto.controller';
import { Producto } from './entities/producto.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoIvaModule } from "../tipo-iva/tipo-iva.module";
import { TipoIvaService } from 'src/tipo-iva/tipo-iva.service';
import { TipoIva } from 'src/tipo-iva/entities/tipo-iva.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Producto]),
    TypeOrmModule.forFeature([TipoIva]),
    TipoIvaModule
  ],
  controllers: [ProductoController],
  providers: [ProductoService, TipoIvaService]
})
export class ProductoModule { }
