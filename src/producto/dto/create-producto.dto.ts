import { IsNotEmpty } from "class-validator";

export class CreateProductoDto {

    id?: number;
    
    @IsNotEmpty()
    nombre_producto: String;

    descripcion?: String;

    @IsNotEmpty()
    precio_sin_iva: number;

    precio_con_iva?: number;

    @IsNotEmpty()
    id_tipo_iva: number;

    fecha_registro?: Date;
}
