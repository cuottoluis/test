import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, NotAcceptableException } from '@nestjs/common';
import { UsuarioService } from "./../usuario/usuario.service";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private usuarioService: UsuarioService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: `${process.env.JWT_SECRET}`
        });
    }

    async validate(payload: any) {
        const { sub: id } = payload;
        if (payload.hasOwnProperty('passwordRecoveryPin')) {
            throw new NotAcceptableException('Jwt token is invalid.')
        }
        return await this.usuarioService.findOne(id);
    }
}