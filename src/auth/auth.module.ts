import { UsuarioModule } from "./../usuario/usuario.module";
import { UsuarioService } from "./../usuario/usuario.service";
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Usuario } from "src/usuario/entities/usuario.entity";

@Module({
	imports: [
		TypeOrmModule.forFeature([Usuario]),
		PassportModule,
		JwtModule.registerAsync({
			inject: [ConfigService],
			useFactory: (config: ConfigService) => ({
				secret: config.get<string>('JWT_SECRET'),
				signOptions: { expiresIn: config.get<string>('JWT_EXPIRED') },
			}),
		}),
		UsuarioModule
	],
	controllers: [AuthController],
	providers: [AuthService, UsuarioService, JwtStrategy]
})
export class AuthModule { }
