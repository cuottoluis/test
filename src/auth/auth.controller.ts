import { LoginAuthDto } from './dto/login.dto';
import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegistroDto } from "./dto/registro.dto";


@Controller('auth')
export class AuthController {
	constructor(private readonly authService: AuthService) { }

	@Post('login')
	login(@Body() user: LoginAuthDto) {
		return this.authService.login(user);
	}

	@Post('registro')
	registro(@Body() usuarioRegistro: RegistroDto) {
		return this.authService.registroUsuario(usuarioRegistro);
	}
}
