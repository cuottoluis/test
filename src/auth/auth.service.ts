import { Injectable, NotAcceptableException, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsuarioService } from "./../usuario/usuario.service";
import { LoginAuthDto } from './dto/login.dto';
import { RegistroDto } from './dto/registro.dto';

@Injectable()
export class AuthService {

	constructor(
		private usuarioService: UsuarioService,
		private jwtService: JwtService
	) { }

	async registroUsuario(usuarioRegistro: RegistroDto) {
		try {
			var nuevoUsuario = await this.usuarioService.create(usuarioRegistro);
			if (nuevoUsuario) {
				if(nuevoUsuario){
					return true;
				}
			} else {
				throw new NotAcceptableException('Usuario no registrado');
			}
		} catch (error) {
			return error;
		}
	}


	async login(usuarioLogin: LoginAuthDto) {
		const usuario = await this.usuarioService.findByEmail(usuarioLogin.email);
		if (usuario) {
			const validarUsuario = await this.usuarioService.validarUsuario(usuario.id, usuarioLogin.password);
			if (validarUsuario) {
				var accessToken = this.jwtService.sign({ sub: usuario.id })
				var retorno = {
					usuarioId: usuario.id,
					nombres: usuario.nombres,
					email: usuario.email,
					logeado: validarUsuario,
					accessToken: accessToken
				}
				return retorno
			} else {
				throw new NotAcceptableException('Usuario o password invalidos');
			}
		} else {
			throw new NotAcceptableException('Usuario no encontrado');
		}
	}
}