import { IsAlphanumeric, IsEmail, IsInt, IsNotEmpty, IsOptional } from "class-validator";

export class LoginAuthDto {
    @IsNotEmpty()
    @IsEmail()
    email;

    @IsNotEmpty()
    password: string;
}
