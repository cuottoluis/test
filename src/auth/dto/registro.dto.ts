import { IsAlphanumeric, IsEmail, IsInt, IsNotEmpty, IsOptional } from "class-validator";

export class RegistroDto {
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    nombres: string;
}
