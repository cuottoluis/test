import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private usuarioRepository: Repository<Usuario>
  ) { }

  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    try {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(createUsuarioDto.password, salt);
      createUsuarioDto.password = hash;
      var nuevoUsuario = await this.usuarioRepository.save(createUsuarioDto);
      return nuevoUsuario;
    } catch (error) {
      return error;
    }
  }

  async findAll(): Promise<Usuario[]> {
    try {
      return await this.usuarioRepository.find();
    } catch (error) {
      return error;
    }
  }

  async findOne(id: number): Promise<Usuario> {
    try {
      return await this.usuarioRepository.findOne(id);
    } catch (error) {
      return error;
    }
  }

  async findByEmail(email: string): Promise<Usuario> {
    try {
      return await this.usuarioRepository.findOne({ where: { email: email } });
    } catch (error) {
      return error;
    }
  }

  async validarUsuario(id, password) {
    try {
      var data = await this.usuarioRepository.findOne(id);
      const ismatch = await bcrypt.compare(password, data.password.toString())
      return (ismatch)
    } catch (error) {
      return error;
    }
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    return `This action updates a #${id} usuario`;
  }

  remove(id: number) {
    return `This action removes a #${id} usuario`;
  }
}
