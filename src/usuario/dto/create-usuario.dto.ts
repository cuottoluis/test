import { IsEmail, IsNotEmpty } from "class-validator";
export class CreateUsuarioDto {

    id?: number;
    
    @IsNotEmpty()
    @IsEmail()
    email: String;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    nombres: string;

}
