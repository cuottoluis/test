import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Usuario {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public nombres: String;

    @Column({ unique: true })
    public email: String;

    @Column({ select: false })
    public password: String;
}
