import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TipoIva {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public descripcion_iva: String;

    @Column()
    public valor_porcentaje_iva: number;

    @Column({ default: 'now()' })
    public fecha_registro: Date;
}
