import { Test, TestingModule } from '@nestjs/testing';
import { TipoIvaController } from './tipo-iva.controller';
import { TipoIvaService } from './tipo-iva.service';

describe('TipoIvaController', () => {
  let controller: TipoIvaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TipoIvaController],
      providers: [TipoIvaService],
    }).compile();

    controller = module.get<TipoIvaController>(TipoIvaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
