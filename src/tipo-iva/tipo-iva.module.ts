import { Module } from '@nestjs/common';
import { TipoIvaService } from './tipo-iva.service';
import { TipoIvaController } from './tipo-iva.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoIva } from './entities/tipo-iva.entity';

@Module({
  imports:[TypeOrmModule.forFeature([TipoIva])],
  controllers: [TipoIvaController],
  providers: [TipoIvaService]
})
export class TipoIvaModule {}
