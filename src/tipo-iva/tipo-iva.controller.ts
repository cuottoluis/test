import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { TipoIvaService } from './tipo-iva.service';
import { CreateTipoIvaDto } from './dto/create-tipo-iva.dto';
import { UpdateTipoIvaDto } from './dto/update-tipo-iva.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@Controller('tipo-iva')
export class TipoIvaController {
  constructor(private readonly tipoIvaService: TipoIvaService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createTipoIvaDto: CreateTipoIvaDto) {
    return this.tipoIvaService.create(createTipoIvaDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll() {
    return this.tipoIvaService.findAll();
  }

  //@UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.tipoIvaService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTipoIvaDto: UpdateTipoIvaDto) {
    return this.tipoIvaService.update(+id, updateTipoIvaDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.tipoIvaService.remove(+id);
  }
}
