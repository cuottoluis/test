import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTipoIvaDto } from './dto/create-tipo-iva.dto';
import { UpdateTipoIvaDto } from './dto/update-tipo-iva.dto';
import { TipoIva } from './entities/tipo-iva.entity';

@Injectable()
export class TipoIvaService {

  constructor(
    @InjectRepository(TipoIva)
    private tipoIvaRepository: Repository<TipoIva>
  ) { }
  
  async create(createTipoIvaDto: CreateTipoIvaDto) {
    try {
      var nuevoTipoIva = await this.tipoIvaRepository.save(createTipoIvaDto);
      return nuevoTipoIva;
    } catch (error) {
      return error;
    }
  }

  async findAll(): Promise<TipoIva[]> {
    try {
      return await this.tipoIvaRepository.find();
    } catch (error) {
      return error;
    }
  }

  async findOne(id: number): Promise<TipoIva> {
    try {
      return await this.tipoIvaRepository.findOne(id);
    } catch (error) {
      return error;
    }
  }

  update(id: number, updateTipoIvaDto: UpdateTipoIvaDto) {
    return `This action updates a #${id} tipoIva`;
  }

  remove(id: number) {
    return `This action removes a #${id} tipoIva`;
  }
}
