import { Test, TestingModule } from '@nestjs/testing';
import { TipoIvaService } from './tipo-iva.service';

describe('TipoIvaService', () => {
  let service: TipoIvaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TipoIvaService],
    }).compile();

    service = module.get<TipoIvaService>(TipoIvaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
