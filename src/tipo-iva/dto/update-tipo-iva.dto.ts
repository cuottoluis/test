import { PartialType } from '@nestjs/swagger';
import { CreateTipoIvaDto } from './create-tipo-iva.dto';

export class UpdateTipoIvaDto extends PartialType(CreateTipoIvaDto) {}
