import {IsNotEmpty } from "class-validator";

export class CreateTipoIvaDto {
    
    id?: number;

    @IsNotEmpty()
    descripcion_iva: String;

    @IsNotEmpty()
    valor_porcentaje_iva: number;
    
    fecha_registro?: Date;
}
